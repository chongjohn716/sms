(function () {
  const bodyEl = document.querySelector('.markdown-body')
  const contentsEl = document.querySelector('.markdown-contents')
  if (!bodyEl) {
    return
  }

  let root = {
    level: -1,
    children: []
  }

  let lastHeading = root;

  let cid = 0;
  marked.use({
    renderer: {
      heading(text, level) {
        const name = `md-anchor-lv${level}-${cid++}`
        const heading = { level, text: text.replace(/<[^>]*>/g, ''), name, children: [], parent: null }
        let tmp = lastHeading
        while (tmp.level >= level) {
          tmp = tmp.parent
        }
        heading.parent = tmp;
        tmp.children.push(heading);
        lastHeading = heading

        return `
          <h${level}>
            <a name="${name}" class="anchor"></a>
            ${text}
          </h${level}>
        `;
      },
      image(href, title, text){
        if(!title) {
          return `<img src="${href}" alt="${text}">`
        }

        return `
          <div class="image-block">
            <div>
              <img src="${href}" alt="${text}" title="${title}">
              <div>${title}</div>
            </div>
          </div>
        `
      }
    },
  })

  function headingContentsHtml(headingList) {
    return headingList.map(el => {
      const { level, name, text, children } = el;
      return `<div class="contents-item">
        <div><a href="#${name}" class="text-muted level-${level}">${text}</a></div>
        ${children && children.length ? `<div class="contents-children">${headingContentsHtml(children)}</div>` : ''}
      </div>`
    }).join('')
  }

  window.loaderMarkdown = (name) => {
    fetch(`./static/md/${name}.md`).then(res => {
      return res.text()
    }).then(text => {
      const html = marked.parse(text)
      bodyEl.innerHTML = html;
      contentsEl.innerHTML = headingContentsHtml(root.children)
    })
  }
})()

